<cfset qryData = queryNew("ProjectID,ProjectDescription,WorkScope,Innovation,AddedValue")>
<cfset queryAddRow(qryData, 1)>
<cfset querySetCell(qryData, "ProjectID", "9492200")>
<cfset querySetCell(qryData, "ProjectDescription", "New building for {the California Academy of Sciences (CAS)}, the largest cultural institution in the City of San Francisco founded in 1853 and one of the ten largest natural history museums worldwide.")>
<cfset querySetCell(qryData, "WorkScope", "Structural, mechanical, ""electrical and plumbing engineering, fire consulting, facade engineering, lighting design, sustainability consulting, acoustics consulting and pedestrian planning.")>
<cfset querySetCell(qryData, "Innovation", "very little")>
<cfset querySetCell(qryData, "AddedValue", "About twenty quid.")>
<cfx_CassandraQuery action="insert"
		keyspace="Projects"
		columnFamily="SummarySimple"
		query="qryData"
		keycolumn="ProjectID">

<!--- now try to read it back --->
<cfx_CassandraQuery action="read"
		keyspace="Projects"
		columnFamily="SummarySimple"
		key="5492200"
		name="geoff">
<cfdump var="#geoff#">
