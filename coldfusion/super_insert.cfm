<cfset qryData = queryNew("ProjectID,ProjectDescription,WorkScope,Innovation,AddedValue")>
<cfset queryAddRow(qryData, 1)>
<cfset querySetCell(qryData, "ProjectID", "9492200")>
<cfset querySetCell(qryData, "ProjectDescription", "Some nice descriptive text.")>
<cfset querySetCell(qryData, "WorkScope", "We did lots of work on this project")>
<cfset querySetCell(qryData, "Innovation", "Very little")>
<cfset querySetCell(qryData, "AddedValue", "About twenty quid.")>

<cfset qrySuper = queryNew("key,data")>
<cfset queryAddRow(qrySuper, 1)>
<cfset querySetCell(qrySuper, "key", "test1")>
<cfset querySetCell(qrySuper, "data", qrySuper)>

<cfx_CassandraQuery action="insert"
			keyspace="Keyspace1"
			columnFamily="Super1"
			supercolumn="true"
			query="qrySuper">

<cfx_CassandraQuery action="read"
			keyspace="Keyspace1"
			columnFamily="Super1"
			key="test1"
			name="out">

<cfdump var="#out#">