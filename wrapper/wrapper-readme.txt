Run Cassandra as a Windows Service

Wrapper.exe (http://wrapper.tanukisoftware.org/) allows you to run a Java executable as a Windows service. The files in this archive should be unzipped into {cassandra_home}\bin. 

You should have two files 'wrapper.jar' and this one, in the 'bin' directory and all other files should be in a sub directory 'win32'. You can check the configuration of Wrapper by running 'cassandra.bat' that is inside win32. If this throws no errors, you should be good to run 'InstallService.bat'.

You can configure the details of the installation in wrapper.conf, where the various java options, parameters, as well as the details of the installed service (name, description etc) are set.

Questions? write to us at http://groups.google.com/group/cf-cassandra-discuss