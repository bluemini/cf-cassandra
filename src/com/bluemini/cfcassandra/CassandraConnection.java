package com.bluemini.cfcassandra;

import java.nio.ByteBuffer;

import org.apache.cassandra.thrift.Cassandra;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import com.allaire.cfx.Request;
import com.allaire.cfx.Response;

public class CassandraConnection {

	private String serverName = "localhost";
	private int serverPort = 9160;
	
	protected static ByteBuffer keyspace;
	protected static Long tagTimestamp;
	protected org.apache.thrift.transport.TTransport transport;
	protected TProtocol protocol;
	protected Cassandra.Client client;

	protected void connect(Request request, Response response) throws Exception {
		
		// check the servername has been specified
		if (request.attributeExists("SERVER"))
			serverName = request.getAttribute("SERVER");
		if (request.attributeExists("PORT")) {
			try {
				serverPort = new Integer(request.getAttribute("PORT"))
						.intValue();
			} catch (Exception e) {
				// throw new
				// Exception("Unable to assign the specified port. Ensure that the value specified is an integer."
				// + e.getMessage());
				response.write("Unable to assign the specified port. Ensure that the value specified is an integer."
						+ e.getMessage());
				return;
			}
		}
		
		System.out.println("Attempting to connect to: " + serverName + " on port " + serverPort);

		// try and open a connection to the running Cassandra server..
		TSocket socket = new org.apache.thrift.transport.TSocket(serverName, serverPort);
		TTransport transport = new TFramedTransport(socket);
		protocol = new TBinaryProtocol(transport);
		client = new Cassandra.Client(protocol);
		transport.open();
		tagTimestamp = System.currentTimeMillis();

	}
	
}
