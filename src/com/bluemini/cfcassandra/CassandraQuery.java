/**
 * CassandraQuery is a CFX tag for ColdFusion that encapsulates the complexity of
 * dealing with Apache Cassandra as a back-end database.
 * 
 * @author Nick Harvey
 * 2012 June 12		- Started to update the code to work against the 1.1.1 version of
 * 					  Cassandra. Intermediate updates have made the existing code
 * 					  inoperable. 
 * 
 * @author Nick Harvey
 * 2010 April 15 - Only simple columns are supported and so far only on insert
 * 
 * Usage:
 *     Action        the action to be performed against Cassandra (read|insert|remove)
 *     Keyspace      the Keyspace in which the target columnFamily is located
 *     ColumnFamily  the ColumnFamily to which to apply the action
 *     
 *     Action=read
 *     Key           the key against which the action will performed
 *     Name          the name of the structure in which to return the data
 *     
 *     Action=delete
 *     
 *     Action=insert
 *     Query         Provides the data to be inserted
 *     KeyColumn     determines the column in the query to use as the key. All
 *                     other columns in the query will become columns in Cassandra
 */
package com.bluemini.cfcassandra;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cassandra.thrift.Cassandra;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ColumnPath;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.SuperColumn;

import com.allaire.cfx.CustomTag;
import com.allaire.cfx.Query;
import com.allaire.cfx.Request;
import com.allaire.cfx.Response;

public class CassandraQuery extends CassandraConnection implements CustomTag {

	private Request request;
	private Response response;

	private String action = "";
	private static String columnFamily;

	public void processRequest(Request request, Response response)
			throws Exception {

		this.response = response;
		this.request = request;

		this.connect(request, response);

		// fetch the column family, if it exists
		if (request.attributeExists("COLUMNFAMILY")) {
			columnFamily = request.getAttribute("COLUMNFAMILY");
		} else {
			throw new Exception("You must specify a columnFamily attribute");
		}

		// set the keyspace
		if (request.attributeExists("KEYSPACE")) {
			keyspace = ByteBuffer.wrap(request.getAttribute("KEYSPACE").getBytes("utf-8"));
		} else {
			response.write("You must specify a keyspace value.");
			return;
			// throw new Exception("You must specify a keyspace value.");
		}
		client.set_keyspace(keyspace.toString());
		
		// check that the action attribute is specified and call
		if (!request.attributeExists("ACTION"))
			throw new Exception(
					"You must provide an action attribute. Action must be set to one of the following: read, insert, modify, remove");
		action = request.getAttribute("ACTION").toUpperCase();
		if (action.equals("READ")) {
			response.writeDebug("Calling tryRead");
			tryRead(client);
		} else if (action.equals("INSERT")) {
			response.writeDebug("Calling tryInsert");
			tryInsert(client);
		} else if (action.equals("MODIFY")) {
			response.writeDebug("Calling tryModify");
			tryModify(client);
		} else if (action.equals("REMOVE")) {
			response.writeDebug("Calling tryRemove");
			tryRemove(client);
		} else {
			throw new Exception(
					"An action attribute of '"
							+ action
							+ "' is unrecognized. Action must be set to one of the following: read, insert, modify, remove");
		}

		transport.close();
		response.writeDebug("Success");
	}

	private void tryRead(Cassandra.Client client) throws Exception {
		// read the predicate
		SlicePredicate predicate = new SlicePredicate();
		SliceRange sliceRange = new SliceRange();

		// set the range, based on the specification of range or columns
		if (request.attributeExists("RANGE")) {
			String[] range = request.getAttribute("RANGE").split(",");
			sliceRange.setStart(range[0].getBytes("utf-8"));
			sliceRange.setFinish(new byte[0]);
			if (range.length > 1)
				sliceRange.setFinish(range[1].getBytes("utf-8"));
			predicate.setSlice_range(sliceRange);

		} else if (request.attributeExists("COLUMNS")) {
			List<ByteBuffer> sliceColumns = new ArrayList<ByteBuffer>();
			String[] columns = request.getAttribute("COLUMNS").split(",");
			for (int iCol = 0; iCol < columns.length; iCol++) {
				sliceColumns.add(ByteBuffer.wrap(columns[iCol].getBytes("utf-8")));
			}
			predicate.setColumn_names(sliceColumns);

		} else {
			sliceRange.setStart(new byte[0]);
			sliceRange.setFinish(new byte[0]);
			predicate.setSlice_range(sliceRange);
		}

		// output the data back to a client query
		response.writeDebug("done the parsing of attributes<br>");

		// String[] columns; // holds the column names for the returned query
		ColumnParent parent = new ColumnParent(columnFamily);
		if (request.attributeExists("SUPERCOLUMN"))
			parent.setSuper_column(request.getAttribute("SUPERCOLUMN")
					.getBytes("utf-8"));
		try {
			List<ColumnOrSuperColumn> results = client.get_slice(keyspace,
					parent, 
					predicate,
					ConsistencyLevel.ONE);

			response.writeDebug("done the read<br>");

			// if there are no results, then set columns as empty
			if (results.size() == 0) {
				String[] columns = {"No Data"};
				response.addQuery(request.getAttribute("NAME"), columns);
			} else {
				// fetch the first row and inspect if it is a column or a
				// super_column
				ColumnOrSuperColumn check = results.get(0);
				if (check.column != null) {
					readColumns(results);
				} else if (check.super_column != null) {
					readSuperColumns(results);
				}
			}

			response.writeDebug("done the write to query<br>");

		} catch (InvalidRequestException ire) {
			// throw new Exception("Read failed."+ire.why);
			response.write("Read failed."+ire.why);
			return;
		} catch (Exception e) {
			// throw new Exception("Read failed:" + e.getMessage());
			response.write("Read failed:" + e.getMessage());
			return;
		}

	}

	private void readColumns(List<ColumnOrSuperColumn> columns) {
		String[] columnHeadings = new String[columns.size()];
		int count = 0;

		response.writeDebug("here" + columns.size() + "<br>");
		// construct the column listing
		for (ColumnOrSuperColumn column : columns) {
			// if we received a standard column, then parse
			columnHeadings[count] = column.column.name.toString();
			count++;
		}

		// create the query object
		Query query = response.addQuery(request.getAttribute("NAME"),
				columnHeadings);
		query.addRow();

		// fill the query with the data
		count = 1; // the query structure is 1-indexed
		for (ColumnOrSuperColumn column : columns) {
			// if we received a standard column, then parse
			query.setData(1, count, column.column.value.toString());
			count++;
		}
	}

	private void readSuperColumns(List<ColumnOrSuperColumn> superColumn) {
		List<Column> columns = superColumn.get(0).super_column.columns;
		String[] columnHeadings = new String[columns.size()];
		int count = 0;

		response.writeDebug("here" + columns.size() + "<br>");
		// construct the column listing
		for (Column column : columns) {
			// if we received a standard column, then parse
			columnHeadings[count] = column.name.toString();
			count++;
		}

		// create the query object
		Query query = response.addQuery(request.getAttribute("NAME"),
				columnHeadings);
		query.addRow();

		// fill the query with the data
		count = 1;
		for (Column column : columns) {
			// if we received a standard column, then parse
			query.setData(1, count, column.value.toString());
			count++;
		}
	}

	private void tryInsert(Cassandra.Client client) throws Exception {
		response.writeDebug("In tryInsert<br>");
		if (request.attributeExists("DATA") && request.attributeExists("KEY")) {
			tryInsertData(client);
		} else if (request.getQuery() != null
				&& request.attributeExists("KEYCOLUMN")) {
			tryInsertQuery(client);
		} else {
			throw new Exception(
					"When inserting you must use either a combination of data/key or query/keycolumn");
		}
	}

	private void tryInsertData(Cassandra.Client client) throws Exception {
		Object dataObject = request.getAttribute("DATA");
		response.writeDebug("Data: " + dataObject + "<br>");
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param client
	 * @throws Exception
	 */
	private void tryInsertQuery(Cassandra.Client client) throws Exception {
		response.writeDebug("inside tryInsertQuery");
		Query query = request.getQuery();
		if (query == null)
			throw new Exception(
					"The query attribute must contain a ColdFusion query object.");

		if (request.attributeExists("SUPERCOLUMN")) {
			tryInsertQuerySuper(client, query);
		} else {
			tryInsertQueryStandard(client, query);
		}
	}

	/**
	 * Performs the insertion of a query into a standard (non super) column.
	 * Each row in the incoming query is inserted under a unique key, which is
	 * established by the parameter passed in as 'keycolumn'.
	 * 
	 * @param request
	 * @param response
	 * @param client
	 * @throws Exception
	 */
	private void tryInsertQueryStandard(Cassandra.Client client, Query query)
			throws Exception {
		response.writeDebug("inside tryInserQueryStandard<br>");

		// get the keycolumn value and verify it's in the data
		String keyColumn = request.getAttribute("KEYCOLUMN");
		ByteBuffer keyName = ByteBuffer.wrap("".getBytes());
		int keyColumnId = query.getColumnIndex(keyColumn);
		if (keyColumnId == 0)
			throw new Exception("The provided keyColumn value '" + keyColumn
					+ "' does not exist in the query provided.");

		// set up the various holders for constructing the data to be inserted
		int nRows = query.getRowCount();
		String[] cols = query.getColumns();
		Map<ByteBuffer, Map<String, List<Mutation>>> job = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();

		// loop over each row in the query
		try {
			for (int iRow = 1; iRow <= nRows; iRow++) {

				List<Mutation> mutations = new ArrayList<Mutation>();
				Map<String, List<Mutation>> columnFamilyMutations = new HashMap<String, List<Mutation>>();

				// create the column collections for the key
				for (int iCol = 0; iCol < cols.length; iCol++) {

					// ignore the column that is the keycolumn
					if (iCol + 1 != keyColumnId) {
						ColumnOrSuperColumn cassCol = new ColumnOrSuperColumn();
						Column c = new Column();
						c.setName(cols[iCol].getBytes("utf-8"));
						c.setValue(query.getData(iRow, iCol + 1).getBytes("utf-8"));
						c.setTimestamp(tagTimestamp);
						cassCol.setColumn(c);

						Mutation mutation = new Mutation();
						mutation.setColumn_or_supercolumn(cassCol);

						// add the column to the List
						mutations.add(mutation);
						columnFamilyMutations.put(columnFamily, mutations);
					} else {
						keyName = ByteBuffer.wrap(query.getData(iRow, iCol + 1).getBytes("utf-8"));
					}
				}

				// add the List to the job
				job.put(keyName, columnFamilyMutations);

			}

			// run the insert
			try {
				// client.batch_mutate(keyspace, job, ConsistencyLevel.ONE);
				client.batch_mutate(job, ConsistencyLevel.ONE);
				
				// client.batch_insert("ArupProjects", "OA5492200", job,
				// ConsistencyLevel.ONE);
			} catch (InvalidRequestException ire) {
				throw new Exception("You have made an invalid request."
						+ ire.why);
			} catch (Exception e) {
				throw new Exception(
						"An error occurred during the batch_mutate operation (keyspace="
								+ request.getAttribute("keyspace")
								+ e.getMessage());
			}

		} catch (Exception e) {
			throw new Exception("An error occurred during the insert. "
					+ e.getMessage());
		}

		response.writeDebug("Completed the insert<br>");
	}

	/**
	 * Each row in the incoming query will be inserted into the columnFamily
	 * using the key value specified in the query in the column defined as
	 * 'keycolumn'. Each supercolumn will have the same name, as defined by the
	 * 'supercolumn' attribute.
	 * 
	 * @param request
	 * @param response
	 * @param client
	 * @param query
	 * @throws Exception
	 */
	private void tryInsertQuerySuper(Cassandra.Client client, Query query)
			throws Exception {
		response.writeDebug("inside tryInserQuerySuper");
		// get the keycolumn value and verify it's in the data
		String keyColumnName = request.getAttribute("KEYCOLUMN").toUpperCase();
		ByteBuffer keyColumn = ByteBuffer.wrap("".getBytes());
		int keyColumnId = query.getColumnIndex(keyColumnName);
		if (keyColumnId <= 0)
			throw new Exception("The provided keyColumn value '"
					+ keyColumnName + "' does not exist in the query provided.");

		// extract the name of the supercolumn
		String superColumnName = request.getAttribute("SUPERCOLUMN");

		// set up the various holders for constructing the data to be inserted
		int nRows = query.getRowCount();
		String[] cols = query.getColumns();
		Map<ByteBuffer, Map<String, List<Mutation>>> job = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();

		/*
		 * loop over each row in the query. Each row represents a super column
		 * and will be inserted as a supercolumn under the key which is found in
		 * the column named 'keyname'. The name of the supercolumn will be based
		 * on the attribute 'supername'.
		 */
		int iRow = 0;
		try {
			for (iRow = 1; iRow <= nRows; iRow++) {

				List<Column> columns = new ArrayList<Column>();
				List<Mutation> mutations = new ArrayList<Mutation>();
				Map<String, List<Mutation>> columnFamilyMutations = new HashMap<String, List<Mutation>>();

				// create the column collections for the key
				for (int iCol = 0; iCol < cols.length; iCol++) {

					// ignore the column that is the keycolumn
					if (iCol + 1 != keyColumnId) {
						if (query.getData(iRow, iCol + 1) != null) {
							Column c = new Column();
							c.setName(cols[iCol].getBytes("utf-8"));
							c.setValue(query.getData(iRow, iCol + 1).getBytes("utf-8"));
							c.setTimestamp(tagTimestamp);
							columns.add(c);
						} else {
							Column c = new Column();
							c.setName(cols[iCol].getBytes("utf-8"));
							c.setValue("".getBytes("utf-8"));
							c.setTimestamp(tagTimestamp);
							columns.add(c);
						}
					} else {
						keyColumn = ByteBuffer.wrap(query.getData(iRow, iCol + 1).getBytes("utf-8"));
					}
				}

				// create the supercolumn and assign the columns
				try {
					ColumnOrSuperColumn superCol = new ColumnOrSuperColumn();
					SuperColumn sc = new SuperColumn();
					sc.setName(superColumnName.getBytes("utf-8"));
					sc.setColumns(columns);
					superCol.setSuper_column(sc);

					Mutation mutation = new Mutation();
					mutation.setColumn_or_supercolumn(superCol);
					mutations.add(mutation);

					columnFamilyMutations.put(columnFamily, mutations);

					// add the List to the job
					job.put(keyColumn, columnFamilyMutations);
				} catch (Exception e) {
					throw new Exception(
							"error assigning columsn to the supercolumn container.");
				}

			}

			// run the insert
			try {
				client.batch_mutate(job, ConsistencyLevel.ONE);
				// client.batch_insert("ArupProjects", "OA5492200", job,
				// ConsistencyLevel.ONE);
			} catch (InvalidRequestException ire) {
				throw new Exception("You have made an invalid request."
						+ ire.why);
			} catch (Exception e) {
				throw new Exception(
						"An error occurred during the batch_insert operation (keyspace="
								+ request.getAttribute("keyspace") + ", key="
								+ query.getData(iRow, keyColumnId) + ", job="
								+ job + " " + e.getMessage());
			}

		} catch (Exception e) {
			throw new Exception("An error occurred during the insert. "
					+ e.getMessage());
		}

		response.writeDebug("Completed the insert<br>");
	}

	private void tryModify(Cassandra.Client client) throws Exception {

	}

	private void tryRemove(Cassandra.Client client) throws Exception {
		// if there is no columns specified, then we are deleting either the
		// entire key or
		// supercolumn
		response.writeDebug("Trying to remove<br>");
		if (request.attributeExists("COLUMNS")) {
			tryRemoveOnColumns();
		} else if (request.attributeExists("KEYS")) {
			tryRemoveOnKeys();
		} else if (request.attributeExists("KEY")) {
			response.writeDebug("Trying to remove supercolumn<br>");
			ColumnPath columnPath = new ColumnPath(columnFamily);
			if (request.attributeExists("SUPERCOLUM"))
				columnPath.super_column = ByteBuffer.wrap(request.getAttribute("SUPERCOLUMN").getBytes("utf-8"));
			client.remove(keyspace,
					new ColumnPath(columnFamily), tagTimestamp.longValue(),
					ConsistencyLevel.ONE);
		} else {
			String[] atts = request.getAttributeList();
			for (String att : atts)
				response.writeDebug(att);
			response.writeDebug(new Integer(atts.length).toString());
			response.writeDebug("unable to handle the remove<br>");
		}
	}

	private void tryRemoveOnColumns() {

	}

	private void tryRemoveOnKeys() {

	}

}
