package com.bluemini.cfcassandra.tests;

import java.util.Hashtable;

import com.allaire.cfx.DebugQuery;
import com.allaire.cfx.DebugRequest;
import com.allaire.cfx.DebugResponse;
import com.bluemini.cfcassandra.CassandraQuery;

public class InsertSuperDebug extends CassandraQuery {

	/**
	 * @param args
	 */
    // debugger testbed for OutputQuery
    public static void main(String[] argv) {
        try {
            // initialize attributes 
            Hashtable<String, String> attributes = new Hashtable<String, String>() ;
            attributes.put( "server", "localhost");
            attributes.put( "action", "insert" ) ;
            attributes.put( "keyspace", "Keyspace1" ) ;
            attributes.put( "columnFamily", "Super1" ) ;
            attributes.put( "superColumn", "sc1" ) ;
            attributes.put( "keyColumn", "name" ) ;
 
            // initialize query
            String[] columns = {"name", "age", "BREED", "colour", "weight" } ;
 
            String[][] data = {
            	{ "Maple", "5", "Tabby"  , "grey"  , "20" },
            	{ "Jody" , "6", "Unknown", "ginger", "11" } };
 
            DebugQuery query = new DebugQuery("query", columns, data ) ; 

            // create tag, process debugging request, and print results
            InsertSuperDebug tag = new InsertSuperDebug() ;
            DebugRequest request = new DebugRequest(attributes, query ) ;
            DebugResponse response = new DebugResponse() ;
            
            tag.processRequest( request, response ) ;
            response.printResults() ;
        }
        catch( Throwable e ) {
            e.printStackTrace() ;
        }
    }
}
