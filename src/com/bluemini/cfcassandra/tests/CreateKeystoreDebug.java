package com.bluemini.cfcassandra.tests;

import java.util.Hashtable;

import com.allaire.cfx.DebugRequest;
import com.allaire.cfx.DebugResponse;
import com.bluemini.cfcassandra.CassandraManage;

public class CreateKeystoreDebug extends CassandraManage {

	/**
	 * @param args
	 */
	// debugger testbed for OutputQuery
	public static void main(String[] argv) {
		try {
			// initialize attributes
			Hashtable<String, String> attributes = new Hashtable<String, String>();
			attributes.put("server", "localhost");
			attributes.put("action", "createkeyspace");
			attributes.put("keyspace", "TestKeyspace");

			// initialize query
			/*
			String[] columns = { "name", "age", "breed", "colour", "weight" };

			String[][] data = { { "Maple", "5", "Maine Coone", "grey", "20" },
					{ "Jody", "6", "Tabby", "ginger", "11" } };

			DebugQuery query = new DebugQuery("query", columns, data);
			*/

			// create tag, process debugging request, and print results
			CreateKeystoreDebug tag = new CreateKeystoreDebug();
			DebugRequest request = new DebugRequest(attributes);
			DebugResponse response = new DebugResponse();

			tag.processRequest(request, response);
			response.printResults();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
