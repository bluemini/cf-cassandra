package com.bluemini.cfcassandra.tests;

import java.util.Hashtable;

import com.allaire.cfx.DebugQuery;
import com.allaire.cfx.DebugRequest;
import com.allaire.cfx.DebugResponse;
import com.bluemini.cfcassandra.CassandraQuery;

public class ReadStandardDebug extends CassandraQuery {

	/**
	 * @param args
	 */
    // debugger testbed for OutputQuery
    public static void main(String[] argv) {
        try {
            // initialize attributes 
            Hashtable<String, String> attributes = new Hashtable<String, String>() ;
            attributes.put( "server", "localhost");
            attributes.put( "action", "read" ) ;
            attributes.put( "keyspace", "Keyspace1" ) ;
            attributes.put( "columnFamily", "Standard1" ) ;
            attributes.put( "name", "mydata");
            attributes.put( "key", "Jody");
 
            // create tag, process debugging request, and print results
            ReadStandardDebug tag = new ReadStandardDebug() ;
            DebugRequest request = new DebugRequest(attributes) ;
            DebugResponse response = new DebugResponse() ;
            
            tag.processRequest( request, response ) ;
            response.printResults() ;
        }
        catch( Throwable e ) {
            e.printStackTrace() ;
        }
    }
}
