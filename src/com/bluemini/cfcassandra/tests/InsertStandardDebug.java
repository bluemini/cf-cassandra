package com.bluemini.cfcassandra.tests;

import java.util.Hashtable;

import com.allaire.cfx.DebugQuery;
import com.allaire.cfx.DebugRequest;
import com.allaire.cfx.DebugResponse;
import com.bluemini.cfcassandra.CassandraQuery;

public class InsertStandardDebug extends CassandraQuery {

	/**
	 * @param args
	 */
    // debugger testbed for OutputQuery
    public static void main(String[] argv) {
        try {
            // initialize attributes 
            Hashtable<String, String> attributes = new Hashtable<String, String>() ;
            attributes.put( "server", "localhost");
            attributes.put( "action", "insert" ) ;
            attributes.put( "keyspace", "Keyspace1" ) ;
            attributes.put( "columnFamily", "Standard1" ) ;
            attributes.put( "keyColumn", "name" ) ;
 
            // initialize query
            String[] columns = {"name", "age", "breed", "colour", "weight" } ;
 
            String[][] data = {
            	{ "Maple", "5", "Maine Coone", "grey", "20" },
            	{ "Jody", "6", "Tabby", "ginger", "11" } };
 
            DebugQuery query = new DebugQuery("query", columns, data ) ; 

            // create tag, process debugging request, and print results
            InsertStandardDebug tag = new InsertStandardDebug() ;
            DebugRequest request = new DebugRequest(attributes, query ) ;
            DebugResponse response = new DebugResponse() ;
            
            tag.processRequest( request, response ) ;
            response.printResults() ;
        }
        catch( Throwable e ) {
            e.printStackTrace() ;
        }
    }
}
