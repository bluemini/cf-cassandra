package com.bluemini.cfcassandra;

/*******************************************
 * Allows for the management of the Cassandra
 * key stores, column tables and super columns.
 * 
 * ACTION=
 * 	createkeyspace:		creates a new keyspace
 * 	
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.cassandra.thrift.CfDef;
import org.apache.cassandra.thrift.KsDef;

import com.allaire.cfx.CustomTag;
import com.allaire.cfx.Request;
import com.allaire.cfx.Response;

public class CassandraManage extends CassandraConnection implements CustomTag {

	private Request request;
	private Response response;

	public void processRequest(Request request, Response response) throws Exception {
		
		this.request = request;
		this.response = response;
		
		// attempt to connect to Cassandra
		connect(request, response);
		
		if (request.attributeExists("ACTION")) {
			String action = request.getAttribute("ACTION");
		} else {
			throw new Exception("You must specify an ACTION attriute when managing Cassandra");
		}
		
		String keyspace = "TestKeyspace";
		
		// create a new keyspace definition
		KsDef ksdef = new KsDef();
		ksdef.setName(keyspace);
		ksdef.setStrategy_class("org.apache.cassandra.locator.SimpleStrategy");
		ksdef.setReplication_factor(1);
		
		Map<String, String> strategyOptions = new HashMap<String, String>();
		strategyOptions.put("replication_factor", "1");
		ksdef.setStrategy_options(strategyOptions);
		
		// create a new column family definition and a new column definition
		List<CfDef> cfdeflist = new ArrayList<CfDef>();
		// List cfdeflist = new ArrayList();
		// CfDef cfdef = new CfDef("TestKeyspace", "family1");

		// create a new column definition
		//ColumnDef column1 = new ColumnDef(ByteBuffer.wrap("column1".getBytes("utf-8")), "UTF8Type");
		
		//cfdef.addToColumn_metadata(column1);
		// cfdeflist.add(cfdef);
		
		ksdef.setCf_defs(cfdeflist);
		
		System.out.println(ksdef);
		
		
		// get keyspaces already existing
		boolean ksfound = false;
		List<KsDef> kss = client.describe_keyspaces();
		Iterator<KsDef> kssiter = kss.iterator();
		while (kssiter.hasNext()) {
			KsDef ks = kssiter.next();
			System.out.println(ks.name);
			if (ks.name.equals(keyspace)) {
				ksfound = true;
			}
		}
		
		if (ksfound) {
			System.out.println("Keystore " + keyspace + " already exists.");
		} else {
			client.system_add_keyspace(ksdef);
			response.writeDebug("Created");
		}
		
		client.system_drop_keyspace(keyspace);
		response.writeDebug("Dropped");
		
	}

}
