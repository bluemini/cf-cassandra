These stats are only based on some random data and on my personal computer. However, they do give some comparitive results.

10 rows			almost instant
100 rows		didn't record this data...!
200 rows		 2100ms
400 rows		 7600ms
500 rows		11700ms
700 rows		23400ms

getting stupid at this point...why is it not proportional? Thought maybe it was the creation of the value string for each column value...

10 rows			almost instant
100 rows		  844ms
200 rows		 2100ms
400 rows		 7700ms
500 rows		11900ms
700 rows		22500ms

didn't make much difference eh! Gotta try a batch_mutate next, this should allow multiple keys to be sent to Cassandra at once. Ok, so I think I've implemented this new method, storing up all inserts into one insert transaction...let's see

10 rows			not done..!
100 rows		   42ms (averaged)
200 rows		   62ms (averaged)
400 rows		  109ms (averaged)
500 rows		  247ms (averaged)
700 rows		  541ms (averaged)

This is MUCH better!!