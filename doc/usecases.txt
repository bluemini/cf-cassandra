Data Tag - CassandraQuery



Structure Tag - CassandraManage

All calls need to specify:
	servername	: the IP/hostname of the server you want to connect to (localhost)
	sererport	: the port of the server you want to connect to (9160)

action = 'addkeyspace'
	Allows you to create a new keyspace in the cluster.
	keyspace	: name of the keyspace to add

action = 'describe'
	keyspace	: the keyspace to describe
	